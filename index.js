// console.log("Hello Universe");
// https://jsonplaceholder.typicode.com/todos


//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.


fetch("https://jsonplaceholder.typicode.com/todos",
		{
			method: "GET",
			headers: {
				"Content-Type":"application/json"
			},
		}
	)
.then(res  => res.json())
.then(json =>{
	json.forEach(post => console.log(post));
});

/*

fetch("https://jsonplaceholder.typicode.com/posts",
		{
			method: "POST",
			headers: {
				"Content-Type":"application/json"
			},
		
		body: JSON.stringify({
			title: "New Post",
			body: "Hellow Universe!",
			userId:1
		})
	}
	)
	.then(response=>response.json())
	.then(json=>console.log(json));

*/

//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.




fetch("https://jsonplaceholder.typicode.com/todos",
		{
			method: "GET",
			headers: {
				"Content-Type":"application/json"
			},
		}
	)
.then(res  => res.json())
.then(json =>{
	json.forEach(post => console.log(post.title));
});


//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/9",
		{
			method: "GET",
			headers: {
				"Content-Type":"application/json"
			},
		}
	)
.then(response=>response.json())
	.then(json=>console.log(json));


//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch("https://jsonplaceholder.typicode.com/todos/9")
.then(response=>response.json())
.then(json => console.log(json.title));


fetch("https://jsonplaceholder.typicode.com/todos/9")
.then(response=>response.json())
.then(json => console.log(json.completed));

//7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos",
		{
			method: "POST",
			headers: {
				"Content-Type":"application/json"
			},
		
		body: JSON.stringify({
			title: "New Post",
			body: "Hello Universe!",
			userId:1
		})
	}
	)
	.then(response=>response.json())
	.then(json=>console.log(json));

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/9",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated This Post",
			body: "Hello updated Universe!",
			userId:1
		})

	})
	.then(response=>response.json())
	.then(json=>console.log(json));

/*

9. Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID

*/

fetch("https://jsonplaceholder.typicode.com/todos/9",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated And Added Something New In This Post",
		description: "Added this description",
		status: "Added this status",
		datecompleted: "Added this date completed",
		userId:1
		})

	})
	.then(response=>response.json())
	.then(json=>console.log(json));

// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/10",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated this post using PATCH method"
		})

	})
	.then(response=>response.json())
	.then(json=>console.log(json));


//11. Update a to do list item by changing the status to complete and add a date when the status was changed.

var today = new Date();

var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = mm + '/' + dd + '/' + yyyy;

fetch("https://jsonplaceholder.typicode.com/todos/12",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: true,
		datecompleted: `status got changed at ${today}`
		})

	})
	.then(response=>response.json())
	.then(json=>console.log(json));


//12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.


fetch("https://jsonplaceholder.typicode.com/todos/13", {method: "DELETE"})
.then(response=>response.json())
.then(json=>console.log(json));

/*

Instructions s33 Activity:



13. Create a request via Postman to retrieve all the to do list items.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as get all to do list items

14. Create a request via Postman to retrieve an individual to do list item.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as get to do list item

15. Create a request via Postman to create a to do list item.
- POST HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as create to do list item


16. Create a request via Postman to update a to do list item.
- PUT HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as update to do list item PUT
- Update the to do list item to mirror the data structure used in the PUT fetch request



17. Create a request via Postman to update a to do list item.
- PATCH HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as create to do list item
- Update the to do list item to mirror the data structure of the PATCH fetch request
//dito ka na


18. Create a request via Postman to delete a to do list item.
- DELETE HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as delete to do list item


19. Export the Postman collection and save it in the a1 folder.
20. Create a git repository named S33.
21. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
22. Add the link in Boodle.
*/